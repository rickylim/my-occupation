# `my-occupation`

Simple java CLI to process DSM occupation of office workspaces.

# Usage

```bash
# Download the excel file as input
# Report for august, september
java -jar target/my-occupation-1.1-spring-boot.jar \
  -i data/202110-occupation.xlsx -o data/202110-occupation.pdf \
  -n ricky -m AUGUST -m September -y 2021
```

# Develop

```bash
mvn build package
```