package utils;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class PdfReport {

    public static void create(String filepath, Map<Month, Set<Integer>> record) {
        int total = 0;
        String presentMarker = "Y";
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);
            PDFont font = PDType1Font.HELVETICA;
            PDPageContentStream content = new PDPageContentStream(doc, page);
            content.setFont(font, 12);
            int row = 0;
            content.beginText();
            content.newLineAtOffset(100, 700 - 20f * row);
            content.showText(String.format("Present that day = %s", presentMarker));
            content.endText();
            ++row;

            List<String> headers = new ArrayList<>();
            for (int i = 0; i <= 31; i++) {
                List<String> texts = new ArrayList<>();
                if (i == 0) {
                    List<String> months = record.keySet().stream().map(Enum::toString).collect(Collectors.toList());
                    texts.add("Day");
                    texts.addAll(months);
                    headers.addAll(texts);
                } else {
                    texts.add(Integer.toString(i));
                    for (Month month : record.keySet()) {
                        if (record.get(month).contains(i)) {
                            texts.add("Y");
                            total++;
                        } else {
                            texts.add(" ");
                        }
                    }
                }
                for (int col = 0; col < texts.size(); col++) {
                    content.beginText();
                    content.newLineAtOffset(100 + 100f * col, 700 - 20f * row);
                    content.showText(texts.get(col));
                    content.endText();
                }
                ++row;
            }
            content.beginText();
            content.newLineAtOffset(100, 700 - 20f * ++row);
            content.showText(String.format("Days present: %d", total));
            content.endText();
            content.close();
            doc.save(filepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
