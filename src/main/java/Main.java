import org.apache.commons.cli.*;
import utils.PdfReport;

import java.io.File;
import java.time.Month;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        Options options = new Options();

        Option input = new Option("i", "input", true, "input EXCEL file");
        input.setRequired(true);
        options.addOption(input);

        Option output = new Option("o", "output", true, "output PDF file");
        output.setRequired(true);
        options.addOption(output);

        Option occupant = new Option("n", "occupant", true, "your name");
        occupant.setRequired(true);
        options.addOption(occupant);

        Option yearOption = new Option("y", "year", true, "year, e.g 2021");
        yearOption.setRequired(true);
        options.addOption(yearOption);

        Option month = new Option("m", "month", true, "month in full, e.g october instead of oct");
        month.setRequired(true);
        options.addOption(month);

        DefaultParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cli;

        try {
            cli = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("my-occupation", options);
            System.exit(1);
            return;
        }

        String inputFile = new File(cli.getOptionValue("input")).getAbsolutePath();
        String outputFile = new File(cli.getOptionValue("output")).getAbsolutePath();
        String occupantName = cli.getOptionValue("occupant");
        String[] months = cli.getOptionValues("month");
        int year = Integer.parseInt(cli.getOptionValue("year"));

        report(inputFile, outputFile, occupantName, months, year);
    }

    public static void report(String input, String output, String occupant, String[] months, int year) {
        List<Month> monthSet = getMonths(months);
        OccupantRecord occupantRecord = new OccupantRecord(input, occupant, year);
        occupantRecord.process(monthSet);
        Map<Month, Set<Integer>> record = occupantRecord.getRecords();
        PdfReport.create(output, record);
    }

    public static List<Month> getMonths(String[] months){
        List<Month> result = new ArrayList<>();
        for (String month : months) {
            Month m = getMonth(month.toUpperCase());
            result.add(m);
        }
        return result;
    }

    public static Month getMonth(String month) {
        switch (month) {
            case "JANUARY":
                return Month.JANUARY;
            case "FEBRUARY":
                return Month.FEBRUARY;
            case "MARCH":
                return Month.MARCH;
            case "APRIL":
                return Month.APRIL;
            case "MAY":
                return Month.MAY;
            case "JUNE":
                return Month.JUNE;
            case "JULY":
                return Month.JULY;
            case "AUGUST":
                return Month.AUGUST;
            case "SEPTEMBER":
                return Month.SEPTEMBER;
            case "OCTOBER":
                return Month.OCTOBER;
            case "NOVEMBER":
                return Month.NOVEMBER;
            case "DECEMBER":
                return Month.DECEMBER;
            default:
                throw new IllegalArgumentException(String.format("%s is not recognized", month));
        }
    }
}
