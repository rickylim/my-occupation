import org.apache.poi.ss.usermodel.Sheet;
import parsers.Floor;
import parsers.ParserFactory;
import parsers.ParsingStrategy;
import tech.tablesaw.api.Table;
import utils.SheetSelector;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OccupantRecord {
    private final Map<Month, Set<Integer>> record;
    private final String filepath;
    private final String occupant;
    private final int year;
    private final Predicate<String> sheetSelector;
    private final ParserFactory parserFactory;


    public OccupantRecord(String filepath, String occupant, int year) {
        this.occupant = occupant;
        this.filepath = filepath;
        this.year = year;
        record = new HashMap<>();
        sheetSelector = name -> Floor.findByName(name) != null;
        parserFactory = new ParserFactory();
    }

    public Map<Month, Set<Integer>> getRecords() {
        return Collections.unmodifiableMap(record);
    }

    public Set<Integer> getDaysByMonth(Month month) {
        HashSet<Integer> result = new HashSet<>();
        List<Sheet> sheets = SheetSelector.getSheets(filepath, sheetSelector);

        for (Sheet sheet : sheets) {
            Floor floor = Floor.findByName(sheet.getSheetName());
            ParsingStrategy parser = parserFactory.createParser(floor);
            Table table = parser.parse(sheet);
            Set<Integer> days = table.columns().stream()
                    .filter(c -> LocalDateTime.parse(c.name()).getYear() == year)
                    .filter(c -> LocalDateTime.parse(c.name()).getMonth().equals(month))
                    .filter(c -> c.anyMatch(o -> o.toString().equalsIgnoreCase(occupant)))
                    .map(c -> LocalDateTime.parse(c.name()).getDayOfMonth())
                    .collect(Collectors.toSet());
            result.addAll(days);
        }
        return result;
    }

    public void process(Month month) {
        Set<Integer> days = getDaysByMonth(month);
        record.put(month, days);
    }

    public void process(List<Month> months) {
        for (Month month : months) {
            process(month);
        }
    }
}
