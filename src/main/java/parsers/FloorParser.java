package parsers;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellUtil;
import tech.tablesaw.api.StringColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.columns.Column;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class FloorParser implements ParsingStrategy {

    public TableCoordinate findTableCoordinate(Sheet sheet) {
        TableCoordinate result = new TableCoordinate();
        for (Row row : sheet) {
            for (Cell cell : row) {
                // Start with cells of date
                if (cell.getCellType() == CellType.NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                    result.startRow = result.startRow == -1 ? row.getRowNum() : result.startRow;
                    result.startCol = result.startCol == -1 ? cell.getColumnIndex() : result.startCol;
                }
                // End with cells one row before 'Desks available'
                if (cell.getCellType() == CellType.STRING && cell.getStringCellValue()
                        .equalsIgnoreCase("Desks available")) {
                    result.endRow = row.getRowNum() - 1;
                    result.endCol = row.getLastCellNum() - 1;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public Table parse(Sheet sheet) {
        Table result = Table.create(sheet.getSheetName());
        TableCoordinate coordinate = findTableCoordinate(sheet);
        Set<String> columnNames = new HashSet<>();

        Map<Integer, StringColumn> columnMap = new HashMap<>();
        for (int i = coordinate.startRow; i <= coordinate.endRow; i++) {
            Row row = CellUtil.getRow(i, sheet);
            for (int j = coordinate.startCol; j <= coordinate.endCol; j++) {
                Cell cell = row.getCell(j, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                if (i == coordinate.startRow && cell != null) {
                    String date = cell.getLocalDateTimeCellValue().toString();
                    StringColumn column = StringColumn.create(date);
                    if (!columnNames.contains(date)) {
                        columnNames.add(date);
                        columnMap.put(j, column);
                    }
                } else {
                    String content = cell != null && cell.getCellType() == CellType.STRING ? cell.getStringCellValue(): "";
                    if (columnMap.get(j) != null) {
                        columnMap.get(j).append(content);
                    }
                }
            }
        }
        result.addColumns(columnMap.values().toArray(new Column[0]));
        return result;
    }
}
