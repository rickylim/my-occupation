package parsers;

import org.apache.poi.ss.usermodel.Sheet;
import tech.tablesaw.api.Table;

public interface ParsingStrategy {
    Table parse(Sheet sheet);
}
