package parsers;

import java.util.HashMap;
import java.util.Map;

public enum Floor {
    GROUND("RFBC groundfloor"),
    One("RFBC 1st floor"),
    TWO("RFBC 2nd floor"),
    THREE("RFBC 3rd floor");

    private static final Map<String, Floor> map = new HashMap<>();

    static {
        for (Floor floor : Floor.values()) {
            map.put(floor.name, floor);
        }
    }

    public final String name;

    Floor(String name) {
        this.name = name;
    }

    public static Floor findByName(String name) {
        return map.get(name);
    }

}
