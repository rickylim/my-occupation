package parsers;


public class ParserFactory {
    public ParsingStrategy createParser(Floor floor) {
        switch (floor) {
            case GROUND:
                return new GroundFloorParser();
            case One:
                return new FirstFloorParser();
            case TWO:
                return new SecondFloorParser();
            case THREE:
                return new ThirdFloorParser();
            default:
                return null;
        }
    }
}
