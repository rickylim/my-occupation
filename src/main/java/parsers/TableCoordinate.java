package parsers;

public class TableCoordinate {
    public int startRow = -1;
    public int startCol = -1;
    public int endRow = -1;
    public int endCol = -1;

    @Override
    public String toString() {
        return "TableCoordinate{" +
                "startRow=" + startRow +
                ", startCol=" + startCol +
                ", endRow=" + endRow +
                ", endCol=" + endCol +
                '}';
    }
}
