import org.apache.poi.ss.usermodel.Sheet;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import parsers.Floor;
import utils.SheetSelector;

import java.io.File;
import java.time.Month;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Nested
class OccupantRecordTest {
    String path = "src/test/resources/202110.xlsx";
    String filepath = new File(path).getAbsolutePath();
    int year = 2021;
    OccupantRecord occupantRecord = new OccupantRecord(filepath, "Tesla", year);

    @Test
    void getSheets() {
        Predicate<String> selector = name -> Floor.findByName(name) != null;
        List<Sheet> sheets = SheetSelector.getSheets(filepath, selector);
        List<String> sheetNames = sheets.stream().map(Sheet::getSheetName).collect(Collectors.toList());
        List<String> expectedSheetNames = Arrays.asList("RFBC groundfloor", "RFBC 1st floor",
                                                        "RFBC 2nd floor", "RFBC 3rd floor");
        assertEquals(expectedSheetNames, sheetNames);
    }

    @Test
    void getDays() {
        Set<Integer> actualTesla = occupantRecord.getDaysByMonth(Month.OCTOBER);
        Set<Integer> expectedTesla = new HashSet<>(Arrays.asList(4, 11, 18, 25, 29));
        assertEquals(expectedTesla, actualTesla);
    }

    @Test
    void process() {
        occupantRecord.process(Month.OCTOBER);
        Map<Month, Set<Integer>> records = occupantRecord.getRecords();
        assertTrue(records.containsKey(Month.OCTOBER));
        HashSet<Integer> expectedTesla = new HashSet<>(Arrays.asList(4, 11, 18, 25, 29));
        assertEquals(expectedTesla, records.get(Month.OCTOBER));
    }

    @Test
    void processMonths() {
        OccupantRecord occupantRecord = new OccupantRecord(filepath, "nadella", year);
        occupantRecord.process(Arrays.asList(Month.OCTOBER, Month.NOVEMBER));
        Map<Month, Set<Integer>> records = occupantRecord.getRecords();
        assertTrue(records.containsKey(Month.OCTOBER));
        assertTrue(records.containsKey(Month.NOVEMBER));
    }

}