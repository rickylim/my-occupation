package utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.Month;
import java.util.*;


class PdfReportTest {
    File outputPath = new File("src/test/resources/pdf");

    @Nested
    class CreatePdf {
        @BeforeEach
        void setUp() {
            if (outputPath.exists()) {
                outputPath.delete();
                outputPath.mkdir();
            }
        }

        @Test
        void createMonthlyPdf() {
            String filepath = new File(outputPath, "2021-10-tesla.pdf").getAbsolutePath();
            Map<Month, Set<Integer>> record = new HashMap<>();
            record.put(Month.OCTOBER, new HashSet<>(Arrays.asList(4, 11, 18, 25, 29)));

            PdfReport.create(filepath, record);
        }

        @Test
        void createMultipleMonthlyPdf() {
            String filepath = new File(outputPath, "2021-11-10-nadella.pdf").getAbsolutePath();
            Map<Month, Set<Integer>> record = new HashMap<>();
            record.put(Month.OCTOBER, new HashSet<>(Arrays.asList(4, 11, 18, 25)));
            record.put(Month.NOVEMBER, new HashSet<>(Arrays.asList(1, 8, 15, 22, 29)));

            PdfReport.create(filepath, record);
        }


    }

}