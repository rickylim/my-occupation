package parsers;

import org.apache.poi.ss.usermodel.Sheet;
import org.junit.jupiter.api.Test;
import tech.tablesaw.api.Table;
import utils.SheetSelector;

import java.io.File;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SecondFloorParserTest {
    String path = "src/test/resources/202110.xlsx";
    String filepath = new File(path).getAbsolutePath();
    Predicate<String> selector = name -> Floor.findByName(name).name.equals("RFBC 2nd floor");
    Sheet sheet = SheetSelector.getSheets(filepath, selector).get(0);
    FloorParser parser = new SecondFloorParser();

    @Test
    void findTableCoordinate() {
        TableCoordinate coordinate = parser.findTableCoordinate(sheet);
        assertEquals(8, coordinate.startRow);
        assertEquals(1, coordinate.startCol);
        assertEquals(44, coordinate.endRow);
        assertEquals(20, coordinate.endCol);
    }

    @Test
    void parse() {
        Table table = parser.parse(sheet);
        assertEquals(20, table.columns().size());
        assertEquals(36, table.rowCount());
    }

}